#!/usr/bin/env python3

from os.path import join, dirname
from setuptools import setup, find_packages

from makesecret.settings import VERSION


REQUIRED_PYTHON = (3, 6)


def readme():
    with open(join(dirname(__file__), 'README.md')) as f:
        return f.read()


setup(
    name='makesecret',
    version=VERSION,
    python_requires='>={}.{}'.format(*REQUIRED_PYTHON),
    description='A generator for secure passwords and passphrases.',
    long_description=readme(),
    long_description_content_type='text/markdown',
    author='Raymond L. Rivera',
    author_email='ray.l.rivera@gmail.com',
    maintainer='Raymond L. Rivera',
    maintainer_email='ray.l.rivera@gmail.com',
    url='https://gitlab.com/ghost-in-the-zsh/makesecret',
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'makesecret=makesecret.entrypoints:main'
        ],
    },
    data_files=[
        ('data', ['makesecret/data/eff_wordlist.db'])
    ],
    install_requires=[
        'makesecretlib@https://gitlab.com/ghost-in-the-zsh/makesecretlib/-/archive/v0.1.1/makesecretlib-v0.1.1.zip',
    ],
    include_package_data=True,
    keywords='secret password passphrase security cryptography entropy generator verification',
    classifiers=(
        # https://pypi.org/classifiers/
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3 Only',
        'Topic :: Security',
        'Topic :: Security :: Cryptography',
        'Topic :: Utilities'
    ),
)
