'''A secrets generator for strong passwords and passphrases

This program locally generates, but does NOT store, user secrets, such as
passwords and passphrases, in a way that is cryptographically strong and
secure. It can also verify secrets for known breaches in a secure way.
However, it is NOT a "password manager". Secure storage is your
responsibility.

Here are the simplest ways to use it for passwords and passphrases:

    $ makesecret password
    ...
    $ makesecret passphrase
    ...
    $ makesecret verify --secret "<secret to check here>"
    ...

Here is how to get help on available options:

    $ makesecret --help
    ...
    $ makesecret password --help
    ...
    $ makesecret passphrase --help
    ...

The passphrase is generated from a words list included as part of the
installation of this program. The words list was created by the Electronic
Frontier Foundation (EFF), and can be found at the following URL:
https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases

The security and strength of this generator is maintained even if an
attacker knows any and/or all of the following:

    * the list of words used,
    * the set of alphanumeric characters used,
    * that this program was used to generate the result,
    * what the source code of this program looks like, and/or
    * the amount of entropy produced by the process

Since this program only generates, but does NOT store, the secret, this
means that SECURE STORAGE of the secrets IS YOUR RESPONSIBILITY.

These are the primary use-cases considered for this tool's implementation
and some recommendations for using it in a way that reduces the chances
of you unknowingly compromising your own security. It is assumed that you
have taken, or will take, the following precautions beforehand:

    * the system is under your full control and is not shared/public,
    * the system is not infected with malware or under surveillance,
    * you have positioned your display to avoid shoulder surfers,
    * you are using secure storage (e.g. a password manager, encrypted
      file system, encrypted browser sync services, etc.)

The strength of passwords and passphrases is measurable, in terms of the
amount of uncertainty, unpredictability, or "randomness" that is built
into the process that generates the result, and not by the result itself.
In other words, the strength of a result is not determined by its own
properties such as length, how "complicated" or "random" it may appear to
the human eye, etc. Rather, it is determined by the amount of (Shannon)
Entropy in the process used to generate it.

In general, Shannon Entropy is measured in terms of the number of options
available to choose from, all of which MUST have the same probability of
being chosen. In short, selection must be TRULY random. But this presents
a problem, because computers are not truly random, they're only pseudo-
random because computers are determistic.

This means that using a Cryptographically Secure Pseudorandom Number
Generator (CSPRNG) is really critical, and that anything that changes the
probabilities when choosing alternatives reduces the amount of entropy
that can be obtained and weakens the whole system. It is also based on a
logarithmic scale, using bits as the unit of measure.[1]

For example, consider a fair coin. The coin provides only 2 possible
outcomes, head or tails, so we say that the the amount of entropy `S = 2`,
because that is the total number of possible choices. To convert this to
the actual measurement in bits, we take the base-2 logarithm of 2, i.e.

    S = 2
    E = log₂(S) = 1

which means there is only 1 bit of entropy. Note that this is only for
a single throw. Every additional throw adds 1 more bit of entropy to the
generated sequence, so a sequence of length `N` would have `N` bits of
entropy. Unfortunately, a process with this little entropy would require
too long a sequence to make it practical. But we can do better. We need
to increase the number of equiprobable alternatives, and thus, the number
of entropy bits per selection attempt.

Consider a fair 8-sided die. In this case:

    S = 8
    E = log₂(S) = 3

That is, the are 8 (or 2³) equally probable outcomes for each throw of
the die. And since `2³ = 8` and `log₂(2³) = 3`, then this shows that the
die offers 3 bits of entropy per throw. In more practical terms, this
means that, while in the first example you would need 12 coin throws to
generate a 12-bit sequence, in the second example you can generate a
sequence with the same 12-bit "strength" with only 4 throws of the die.

By default, `makesecret` generates passwords that are 12 characters long
from a set of 62 symbols including lower/upper-case letters, digits, and
special characters (e.g. punctuation). This means:

    S = 62
    E = log₂(S) ≈ 6

That is, there are about 6 bits of entropy per selection. Since the
default length is 12 characters, that means the total amount of entropy
bits is:

    E = 6 * 12 ≈ 72

which, assuming 1 Billion attempts per second, would take an average of
51 thousand years to brute-force. If the option to include special cha-
racters is used, the extra entropy (about 6.6 bits per choice) would mean
the same attack attempt just mentioned would now need an average of about
7.5 million years instead.

On the other hand, things are different when generating passphrases. The
passphrases are based on a list of common words with 7776 entries in it.
This means:

    S = 7776
    E = log₂(S) ≈ 12.9

showing about 12.9 bits of entropy per selection. Since the default
number of words is 6, that means the total amount of entropy is:

    E = 6 * 12.9 ≈ 77.4

which, under the same assumptions as before, would take an average of
3.5 million years to brute-force. If you think six words is too much to
remember, you can choose a different number. For example:

    $ makesecret passphrase --choose 5

The consequence of this would be:

    E = 5 * 12.9 ≈ 64.6

requiring an average of 4.5 centuries to crack under the same
conditions. To display an analysis of your results, you can use:

    $ makesecret <subcommand> --analyze

Note that all of this relies on selections being made randomly. If you
generate a result using a non-random method or a PRNG that is not Crypto-
graphically Secure, then the strength of your result is going to go down
significantly. In fact, it could have no entropy/strength at all, being
completely predictable, such as "correct horse battery staple"[2].

Secure verification of secrets is done by using the haveibeenpwned.com
API[3] over HTTP + SSL/TLS. The process works as follows:

    1. The password/phrase is locally hashed using the SHA-1 algorithm.
    2. A 5-character prefix of this 40-character hash is sent to the API.
    3. The API responds with a list of hash suffixes matching the given
       prefix (478 suffixes on average).
    4. This program locally looks up the remaining 35-character suffix
       from (2) in the suffix list returned by the API to see if a match
       is found.
    5. When a match is found, additional data from the list shows the
       number of times the cracked hash has been found in known security
       breaches.

Through a mathematical property known as K-Anonimity[4,5], it is possible
to verify the secret without revealing the secret being verified and even
without revealing the complete SHA-1 hash of the secret. This means that
the remote system cannot determine what was being checked, or even if
there was ever a matching suffix to begin with.

Having more than zero matches is always bad for the secret and should be
grounds for rejecting it. However, this means that the secret is *known*
to have been compromised. A lack of matches does NOT mean that the secret
is 100% safe; it only means it was not found in the current dataset and
that we do not *know* of any breaches where it has been found. We cannot
know everything. However, this still means you can have greater confidence
in the strength of your secret, since no evidence of it having been
compromised was found at the time. For example, at the time of this
writing, the password `123456` is known to have appeared 23,174,662
times.

Verification can be done on a specific word or phrase provided by the user
and also for words and phrases generated by this program. For example, this
verifies a word or phrase of your choice:

    $ makesecret verify <your-secret-value>
    ...

Note that if values with spaces must be quoted. For example:

    $ makesecret verify "<secret value with spaces>"
    ...

To verify a randomly generated password or passphrase, you can simply add
the --verify option to the relevant sub-command:

    $ makesecret password --verify
    ...
    $ makesecret passphrase --verify
    ...

When verification attempts find matches, the user is informed very
explicitly.

[1] This is similar to how the Richter Magnitude Scale for earthquakes
works, but using base-2 logarithms instead of base-10 logarithms. What
this means is that, in the same way that an earthquake rated as a 5 in
the scale is 10 times stronger than a 4 and 100 times stronger than a 3
(because it is base-10), a process with 5 bits of entropy has twice as
many choices as one with 4 bits and four times as many choices as one
with 3 bits (because it is base-2). In other words, for every bit of
entropy that you want to add, you must duplicate the total number of
possible choices and make sure that every choice has the same odds of
of being selected.

[2] https://xkcd.com/936

[3] https://haveibeenpwned.com/API/v2

[4] https://en.wikipedia.org/wiki/K-anonymity

[5] https://www.youtube.com/watch?v=hhUb5iknVJs

[6] The command used was `makesecret verify --secret 123456`

'''
