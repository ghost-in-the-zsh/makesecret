import argparse as ap


def valid_positive_number(s: str) -> int:
    '''Validates that input is a positive integer.'''
    msg = 'value must be a positive integer'
    try:
        n = int(s)
        if n <= 0:
            raise ap.ArgumentTypeError(msg)
        return n
    except ValueError:
        raise ap.ArgumentTypeError(msg)
