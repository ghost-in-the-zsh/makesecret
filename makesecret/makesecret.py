#!/usr/bin/env python3

'''Generate cryptographically strong passphrases and passwords'''

import sys
import os

from typing import Text
import argparse as ap
from pkg_resources import resource_filename

from makesecretlib.settings import \
    ATTEMPTS_PER_SECOND, \
    MINIMUM_ENTROPY_BITS, \
    DECIMAL_PRECISION, \
    PASSPHRASE_WORD_COUNT, \
    PASSWORD_CHAR_COUNT

from makesecretlib.providers import PasswordProvider, PassphraseProvider
from makesecretlib.analyzers import StrengthAnalyzer, BreachAnalyzer
from makesecretlib.models import Secret, SecretStrengthMeta, SecretBreachMeta

from makesecret.settings import VERSION
from makesecret.validators import valid_positive_number


_epilog = 'IMPORTANT: Read the documentation to properly understand '   \
          ' how to use this program. In GNU+Linux, you can use '        \
          '`pydoc3 makesecret`. In Windows you may use `py -m pydoc '   \
          'makesecret`.'


_providers = [
    'password',
    'passphrase'
]
_verifiers = [
    'verify'
]


def parse_arguments() -> ap.Namespace:
    '''Parse CLI arguments with an `ArgumentParser`.'''
    parser = ap.ArgumentParser(
        description='A generator for cryptographically strong passwords ' \
                    'and passphrases (and a few other things).',
        formatter_class=ap.ArgumentDefaultsHelpFormatter,
        epilog=_epilog
    )
    parser.add_argument(
        '-v',
        '--version',
        action='version',
        version='%(prog)s {}'.format(VERSION)
    )
    parser.add_argument(
        '--brute-force',
        metavar='ATTEMPTS',
        type=valid_positive_number,
        dest='attacks_per_sec',
        default=ATTEMPTS_PER_SECOND,
        help='assumed guess attempts per second'
    )
    parser.add_argument(
        '--min-entropy',
        metavar='BITS',
        type=valid_positive_number,
        default=MINIMUM_ENTROPY_BITS,
        dest='min_entropy_bits',
        help='minimum amount of entropy bits to consider result acceptable'
    )
    parser.add_argument(
        '--precision',
        metavar='PLACES',
        dest='precision',
        type=valid_positive_number,
        default=DECIMAL_PRECISION,
        help='the number of decimal places to show during analysis'
    )
    subparser = parser.add_subparsers(
        dest='command',
        title='subcommands',
        help='additional help for specific subcommands'
    )
    subparser.required = True
    setup_passphrase_parser(subparser)
    setup_password_parser(subparser)
    setup_verify_parser(subparser)
    return parser.parse_args()


def setup_passphrase_parser(subparser: ap._SubParsersAction) -> None:
    '''Setup passphrase-specific parsing options.'''
    parser = subparser.add_parser(
        'passphrase',
        formatter_class=ap.ArgumentDefaultsHelpFormatter,
        epilog=_epilog,
        help='help for generating passphrases'
    )
    parser.add_argument(
        '--choose',
        metavar='COUNT',
        type=valid_positive_number,
        dest='user_choice_cnt',
        default=PASSPHRASE_WORD_COUNT,
        help='the number of words to choose from the words list'
    )
    parser.add_argument(
        '--words-list',
        metavar='FILE',
        type=str,
        dest='wordlist_file',
        default=resource_filename('makesecret', 'data/eff_wordlist.db'),
        help='a text file with a long list of words (one per line)'
    )
    parser.add_argument(
        '--capitalize-words',
        action='store_true',
        dest='capitalize_words',
        help='capitalize the first letter of every word (does not ' \
             'add more entropy)'
    )
    parser.add_argument(
        '--with-numbers',
        action='store_true',
        dest='with_numbers',
        help='append a random number to each word (adds more entropy)'
    )
    parser.add_argument(
        '--analyze',
        action='store_true',
        dest='show_analysis',
        help='show a simple analysis of your new passphrase'
    )
    parser.add_argument(
        '--verify',
        action='store_true',
        dest='verify_result',
        help='securely check newly generated passphrases against known data braches'
    )


def setup_password_parser(subparser: ap._SubParsersAction) -> None:
    '''Setup password-specific parsing options.'''
    parser = subparser.add_parser(
        'password',
        formatter_class=ap.ArgumentDefaultsHelpFormatter,
        epilog=_epilog,
        help='help for generating passwords'
    )
    parser.add_argument(
        '--choose',
        metavar='COUNT',
        type=valid_positive_number,
        dest='user_choice_cnt',
        default=PASSWORD_CHAR_COUNT,
        help='the number of characters to choose from the alphanumeric set'
    )
    parser.add_argument(
        '--with-special',
        action='store_true',
        dest='with_special',
        help='include special characters in the selection pool '\
             '(adds more entropy, and typing difficulty)'
    )
    parser.add_argument(
        '--analyze',
        action='store_true',
        dest='show_analysis',
        help='show a simple analysis of your new password'
    )
    parser.add_argument(
        '--verify',
        action='store_true',
        dest='verify_result',
        help='securely check newly generated passwords against known data braches'
    )


def setup_verify_parser(subparser: ap._SubParsersAction) -> None:
    '''Setup leaked secret verification parsing options.'''
    parser = subparser.add_parser(
        'verify',
        formatter_class=ap.ArgumentDefaultsHelpFormatter,
        epilog=_epilog,
        help='help for securely checking passwords/passphrases ' \
             'against known data breaches'
    )
    parser.add_argument(
        '--secret',
        metavar='SECRET',
        type=str,
        dest='secret_text',
        help='the secret to securely check against known data breaches; '   \
             'know that your shell will be saving command history to disk ' \
             '(e.g. ~/.bash_history in GNU+Linux), and your secret along with it'
    )


def display_strength_analysis(meta: SecretStrengthMeta) -> None:
    prec = meta.precision

    print()
    print('Facts About Your Result')
    print('-----------------------')
    print('Selection Pool           : {:,}'.format(meta.entropy_base))
    print('Entropy (bits per choice): {:,.{p}f}'.format(meta.entropy_bits, p=prec))
    print('Entropy (bits overall)   : {:,.{p}f}'.format(meta.entropy_bits_total, p=prec))
    print('Entropy (bits effective) : {:,.{p}f}'.format(meta.entropy_bits_effective, p=prec) + os.linesep)

    print('Attack Tolerance')
    print('----------------')
    print('Attack Type: Brute-Force ({:,} attemps/sec assumed)'.format(meta.attacks_per_second))
    print('Guess Attempts (avg.): {:,}'.format(meta.crack_total_count))
    print('Time to Break (avg.)')
    print('  Seconds   : {:,.{p}f}'.format(meta.crack_secs,      p=prec))
    print('  Hours     : {:,.{p}f}'.format(meta.crack_hours,     p=prec))
    print('  Days      : {:,.{p}f}'.format(meta.crack_days,      p=prec))
    print('  Weeks     : {:,.{p}f}'.format(meta.crack_weeks,     p=prec))
    print('  Years     : {:,.{p}f}'.format(meta.crack_years,     p=prec))
    print('  Decades   : {:,.{p}f}'.format(meta.crack_decades,   p=prec))
    print('  Centuries : {:,.{p}f}'.format(meta.crack_centuries, p=prec))
    print('  Millenia  : {:,.{p}f}'.format(meta.crack_millenia,  p=prec))
    print('  Mega-Annum: {:,.{p}f}'.format(meta.crack_millions,  p=prec))
    print('  Giga-Annum: {:,.{p}f}'.format(meta.crack_billions,  p=prec))
    print('Acceptable?: {}'.format(
        'Yes' if meta.entropy_bits_effective >= meta.entropy_bits_minimum else 'No'
    ))


def display_breach_analysis(meta: SecretBreachMeta):
    print()
    print('Verifying Your Secret')
    print('---------------------')
    print(f'Secret           : {meta.secret_text}')
    print(f'Hash (SHA-1)     : {meta.sha1hash}')
    print(f'Hash Prefix      : {meta.sha1hash[:5]}')
    print(f'Hash Suffix      : {meta.sha1hash[5:]}')
    print(f'API Request URL  : {meta.request_url}')
    print('API Results Count: {:,}'.format(meta.candidates_found))
    print('Known Breaches   : {:,}'.format(meta.known_breaches_count))

    if meta.known_breaches_count > 0:
        msg = '!! DANGER !! This secret is compromised! Do NOT use it!'
        print('='*len(msg))
        print(msg)
        print('='*len(msg))


def generate_secret(args: ap.Namespace) -> None:
    if args.command == 'passphrase':
        provider = PassphraseProvider(
            wordslist_file=args.wordlist_file,
            user_choice_cnt=args.user_choice_cnt,
            capitalize_words=args.capitalize_words,
            with_numbers=args.with_numbers
        )
    else:
        provider = PasswordProvider(
            user_choice_cnt=args.user_choice_cnt,
            with_special=args.with_special
        )

    secret = provider.generate()
    print(str(secret))

    if args.show_analysis:
        analysis = StrengthAnalyzer().analyze(
            secret=secret,
            user_choices_count=args.user_choice_cnt,
            attacks_per_second=args.attacks_per_sec,
            entropy_bits_minimum=args.min_entropy_bits,
            precision=args.precision
        )
        display_strength_analysis(analysis)

    if args.verify_result:
        verify_secret(secret)


def verify_secret(secret: Secret) -> None:
    try:
        analysis = BreachAnalyzer().analyze(secret)
        display_breach_analysis(analysis)
    except RuntimeError as e:
        # handle this in a way that avoids cluttering the terminal
        # with user-unfriendly stack-traces; display an easy-to-read
        # message and exit
        print(f'Error: {str(e)}', file=sys.stderr)
        sys.exit(1)


def main():
    args = parse_arguments()

    if args.command in _providers:
        generate_secret(args)
    elif args.command in _verifiers:
        verify_secret(Secret(args.secret_text, 0))

    sys.exit(0)


if __name__ == '__main__':
    main()
