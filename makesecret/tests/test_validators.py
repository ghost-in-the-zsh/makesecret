
from unittest import TestCase
from argparse import ArgumentTypeError

from makesecret.validators import valid_positive_number


class ValidatorTestCase(TestCase):

    def test_positive_value_string_is_accepted(self):
        n = valid_positive_number('1')
        self.assertEqual(1, n)

    def test_negative_value_string_is_rejected(self):
        with self.assertRaises(ArgumentTypeError):
            valid_positive_number('-1')

    def test_zero_value_string_is_rejected(self):
        with self.assertRaises(ArgumentTypeError):
            valid_positive_number('0')
