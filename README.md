# makesecret

A program to generate strong passwords and passphrases, analyze them, and even securely verify if they are known to have been compromised in past breaches based on available datasets. The latter is done ***without ever revealing the secret*** being verified in the first place.

If terminal-based programs are not your thing, you should instead look at [makesecret-qt](https://gitlab.com/ghost-in-the-zsh/makesecret-qt), a PyQt5-based graphical frontend of this program. It has windows, buttons, and other widgets. People like widgets.


## Requirements

* Python v3.6.5 or later
* Python `makesecretlib` module
* A GNU+Linux shell or Windows command prompt

The `makesecretlib` module dependency is automatically installed when installing this program using PIP.


## Installation

You need to use the Python Package Installer. The command syntax may be different depending on the specific platform.


### GNU+Linux

Removing current installation (if any):
```bash
$ pip3 uninstall -y makesecret
```

To install, run this command:
```bash
$ pip3 install https://gitlab.com/ghost-in-the-zsh/makesecret/-/archive/master/makesecret-master.zip
```


### Windows

The syntax for Windows may vary and often depends on whether you have the Python interpreter in your `PATH` or not. Generally, the commands used in the GNU+Linux section are expected to work if you change `pip3 install` to `py -m pip install`. For example:

Removing current installation (if any):
```bash
> py -m pip uninstall -y makesecret
```

Then, to install:
```bash
> py -m pip install https://gitlab.com/ghost-in-the-zsh/makesecret/-/archive/master/makesecret-master.zip
```


## Usage Examples

The general usage structure is:

```
$ makesecret [options] <sub-command> [arguments]
```

Assuming correct installation and environment path, you can also run:
```bash
$ makesecret --help
```

In Windows, you must make sure the Python installation directory for scripts is also in your path (e.g. `C:\Python3\Scripts`); then you simply call the program directly:
```
> makesecret.exe --help
```


### Generating Passphrases

Generate a passphrase using the default number of words
```bash
$ makesecret passphrase
```

Generate a passphrase using a given number of words
```bash
$ makesecret passphrase --choose 6
```

Generate a passphrase that capitalizes words and appends random numbers to them
```bash
$ makesecret passphrase --capitalize-words --with-numbers
```


### Generating Passwords

Generate a password
```bash
$ makesecret password --choose 10
```

Generate a password that includes special characters (i.e. punctuation symbols)
```bash
$ makesecret password --with-special
```


### Result Analysis

You can get a basic analysis of your result with the `--analyze` option:

```bash
$ makesecret <sub-command> --analyze
```

Here is an example, with the actual output redacted:

```bash
$ makesecret password --analyze
<actual password output redacted>

Facts About Your Result
-----------------------
Selection Pool           : 62
Entropy (bits per choice): 5.9542
Entropy (bits overall)   : 71.4504
Entropy (bits effective) : 70.4504

Attack Tolerance
----------------
Attack Type: Brute-Force (1,000,000,000 attemps/sec assumed)
Guess Attempts (avg.): 1,613,133,381,198,938,112,000
Time to Break (avg.)
  Seconds   : 1,613,133,381,198.9382
  Hours     : 448,092,605.8886
  Days      : 18,670,525.2454
  Weeks     : 2,667,217.8922
  Years     : 51,117.1122
  Decades   : 5,111.7112
  Centuries : 511.1711
  Millenia  : 51.1171
  Mega-Annum: 0.0511
  Giga-Annum: 0.0001
Acceptable?: Yes
```
Note that this is an *estimated* measure of the *average*. There's always a chance that an attacker might get lucky and take less time, or unlucky and need more. However, the chances of a lucky attacker succeeding against *randomly generated* secrets are very slim at best.

### Result Verification

Passwords and passphrases, either generated or user-provided, can be checked to see if they're known to have been leaked or cracked before, which would make them unsafe to use. This can be done in a way that is secure ***without*** revealing the actual secret itself.

#### How it Works

The verification process works as follows:

1. This program locally calculates a cryptographic hash[^1] (SHA-1) of the secret.
2. The first 5 characters of this 40-character hash are extracted as a prefix.
3. This hash prefix is sent over secure HTTP (SSL/TLS) to a remote API[^2].
4. The API returns a list of hash suffixes (478 on average) matching the original prefix.
5. This program locally checks if the remaining 35-character hash suffix from (2) match any of the suffixes returned by the API.
6. This program shows its results to the user.

The ***only*** thing that is ever sent over a secure connection is a 5-character hash prefix. The remote API never gets to see the secret being verified nor the full hash that had been locally generated; it only ever sees this prefix. In addition, the remote API never gets to know whether any of the suffixes it returned ever matched anything or not.

For more information, you may want to check this article on [K-Anonimity](https://en.wikipedia.org/wiki/K-anonymity) and this short [Computerphile video](https://www.youtube.com/watch?v=hhUb5iknVJs) to get a better understanding.

#### Verification Examples

Adding the `--verify` option to the `password` and `passphrase` sub-commands will let you check the result that was produced for you just now:
```bash
$ makesecret passphrase --verify
<actual passphrase redacted>

Verifying Your Secret
---------------------
Secret           : <actual passphrase redacted>
Hash (SHA-1)     : aa273<hash suffix redacted>
Hash Prefix      : aa273
Hash Suffix      : <hash suffix redacted>
API Request URL  : https://api.pwnedpasswords.com/range/aa273
API Results Count: 509
Known Breaches   : 0
```

Here are more examples, but ***using secrets that no one should ever be using***. The first one is based on the [well-known XKCD comic](https://www.xkcd.com/936/) on password strength:
```bash
$ makesecret verify --secret "correcthorsebatterystaple"
Verifying Your Secret
---------------------
Secret           : correcthorsebatterystaple
Hash (SHA-1)     : bfd3617727eab0e800e62a776c76381defbc4145
Hash Prefix      : bfd36
Hash Suffix      : 17727eab0e800e62a776c76381defbc4145
API Request URL  : https://api.pwnedpasswords.com/range/bfd36
API Results Count: 521
Known Breaches   : 114
=======================================================
!! DANGER !! This secret is compromised! Do NOT use it!
=======================================================
```

The next one is from the well-known [Spaceballs](https://www.youtube.com/watch?v=a6iW-8xPw3k) movie.
```bash
makesecret verify --secret 12345
Verifying Your Secret
---------------------
Secret           : 12345
Hash (SHA-1)     : 8cb2237d0679ca88db6464eac60da96345513964
Hash Prefix      : 8cb22
Hash Suffix      : 37d0679ca88db6464eac60da96345513964
API Request URL  : https://api.pwnedpasswords.com/range/8cb22
API Results Count: 550
Known Breaches   : 2,333,232
=======================================================
!! DANGER !! This secret is compromised! Do NOT use it!
=======================================================
```

Interestingly, the password `123456`, which people would intuitively think is "more secure" because it's longer, ***is actually worse***:

```bash
$ makesecret verify --secret 123456
Verifying Your Secret
---------------------
Secret           : 123456
Hash (SHA-1)     : 7c4a8d09ca3762af61e59520943dc26494f8941b
Hash Prefix      : 7c4a8
Hash Suffix      : d09ca3762af61e59520943dc26494f8941b
API Request URL  : https://api.pwnedpasswords.com/range/7c4a8
API Results Count: 515
Known Breaches   : 23,174,662
=======================================================
!! DANGER !! This secret is compromised! Do NOT use it!
=======================================================
```

[^1]: Cryptographic hash functions apply one-way transformations to messages. Currently, there are no publicly known mathematical methods to undo such transformations, making the process irreversible. The only way to "crack" hashes is by generating all possible secrets and pre-computing hashes for all of them, and while that is very costly, impractical, and infeasible in general, datasets with pre-computed hashes for *common* user passwords do exist. See [SHA-1](https://en.wikipedia.org/wiki/SHA-1) for more.

[^2]: https://haveibeenpwned.com/API/v2

### Batteries Included

Documentation can be accessed as follows:

In GNU+Linux (and macOS?)
```bash
$ pydoc3 makesecret
```

In Windows
```
> py -m pydoc makesecret
```


## Licensing

This project is [Free Software](https://www.gnu.org/philosophy/free-sw.html) because it respects and protects your freedoms. For a quick summary of what your rights and responsibilities are, you should see [this article](https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3)). For the full text, you can see the [license](LICENSE).
